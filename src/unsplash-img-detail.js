import {LitElement, html, css} from 'lit-element';

export class UnsplashImgDetail extends LitElement {
	static get is() {
		return 'unsplash-img-detail';
	}

	static get properties() {
		return{
			title: {
				type: String
			},
			text: {
				type: String
			}
		};
	}

	static get styles() {
		return css`
		#header {
		  color: #0E376C;
		  font-size: 1.3rem; }

		#description {
		  color: #6D7988; }

		.main {
		  padding-left: 1rem;
		  padding-right: 1rem;
		  padding-top: 0.5rem;
		  text-align: justify; }
		`;
	}

	constructor() {
		super();
		this.title = '';
		this.text = '';
	}

	updated(cp) {
		this._checkTitle();
		this._checkText();
	}

	render() {
		return html`
		<div class="main">
			<h2 id="header" id="title">${this.title}</h2>
			<p id="description" id="text">${this.text}</p>
		</div>
		`;
	}

	// Verifica si el largo de un titulo es demasiado largo o no existe
    _checkTitle() {
      if (! this.title || this.title === 'null') {
        this.setAttribute('title', '');
      }
      if (this.title.length === 0) {
        this.setAttribute('title', 'Titulo no disponible');
      }
      if (this.title.length >= 51) {
        this.setAttribute('title', this.title.substring(0, 50).concat('...'));
      }
    }

    // Verifica la existencia de la descripcion
    _checkText() {
      if (! this.text || this.title === 'null') {
        this.setAttribute('text', '');
      }
      if (this.text.length === 0) {
        this.setAttribute('text', 'Descripción no disponible');
      }
    }
}

customElements.define(UnsplashImgDetail.is, UnsplashImgDetail);